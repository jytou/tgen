Generate many transactions between a number of addresses in a Libre Currency handled by Duniter. This is typically useful to stress the Ğ1-test currency.

# How it works

This is a set of scripts that, used with cron (or any scheduler), makes it possible to generate random transactions between a number of Duniter addresses. This way, many random transactions are generated between accounts, just like in a local economy.

So with a given number of addresses (for which you own the seed, in order to be able to generate transactions from them), the scripts will generate as many transactions as you want/need between those addresses. This way, the money is kept within a closed circle of addresses (think of it as musical chairs), you won't lose any dime, and will always have enough money to circulate between your addresses. Note that the "number of addresses" could theoretically be very large (in fact as large as you wish). And the scripts make it very easy to add new addresses - it's just a matter of adding lines in a file.

The default is to generate a random number of transactions based on the hour of the day, just like in a real economy where there will be a cycle of peak and calm hours. So at 1 in the morning, it may generate 0 to 4 transactions, but at 11pm it may generate up to 27 transactions (or more if you want, you decide!). The amount that is sent between the addresses is also randomly generated, just like in a normal economy where people pay different amounts for different things.

What if one of your addresses has no money in it? The scripts don't verify that beforehand, so they will try to send the transaction. But it will fail, which is a good thing: we rely on the fact that addresses with an insufficient amount of money cannot be used within the system (it relies on silkaj and the nodes to make the correct verifications). If at any point one of your addresses has a negative amount of money, then we found a bug!

Finally, you can also choose the peers to which you wish to send the generated transactions and the load will be spread between those peers. You can also disable some peers if they appear to be faulty.

# What it contains

- amounts.sh: gives the available amounts of all your addresses,
- dispatch.sh: dispatches random transactions, this is the main script that you will run in a cron,
- send.sh: sends one transaction to a random peer.

Extra files used:

- addresses.txt: a copy of the results of vanitygen for the addresses you wish to use,
- peers.txt: the peers you want to use, one per line, you can add a # at the beginning of the line to temporarily stop using a peer,
- silkajpath.txt: one line containing the path to the silkaj runtime on your machine.

# Prerequisites

A Unix system with the basics:

- bash, awk, sed, grep...
- silkaj installed and working, using virtualenvs,
- some units of the currency you want to test.

That's all!

# Setting up

Obviously:

```
git clone https://gitlab.com/jytou/tgen.git
cd tgen
```

If you don't have git, just download the bundle from gitlab and unzip it.

## Pointing to the silkaj installation

In the directory of this program, create a file called silkajpath.txt and paste in it the path to the silkaj executable. For instance:

```
/home/myuser/silkaj/silkaj
```

Make sure that there is only one line in this file and nothing else.
I could have done it with an environment variable, or instructed you to make sure silkaj is in the $PATH, but this seemed like an easy way to do it without disturbing your own environment.

## Addresses

First, you need a few addresses (at least 3, but I would recommend 10 or more - I currently use 100) which will send units to each other. It is best if you use [my address generator](https://gitlab.com/jytou/vanitygen) which will directly give you a file in the correct format. Use the -s switch to generate addresses with seeds only. You will not get the double password authentication that can be used with sakia or cesium, but it is enough for a simple silkaj use and it finds addresses with the desired pattern much faster (hundreds of times faster, actually).

Paste the addresses you want to use in a file called addresses.txt, whose lines are in the format:

```
"pattern" "duniterAddress" "seed"
```

Make sure that there are no empty lines in the file and that all lines correspond to an address you want to use.

Of course, you can add more addresses later if you want in the file, and the scripts will start using them.

## Feeding some addresses

You will want to send some money inside the windmill, otherwise it will have no grain to crunch. So you should send some units to the first test address from one of your "normal" addresses, either via cesium, sakia or silkaj.

## Peers

Collect a list of working peers on the network and paste them into a file called "peers.txt" in the program's directory, one peer on each line. If you don't want to use some peers now but wish to save them for later, just add a "#" at the beginning of the line to comment the peer. Make sure this file doesn't contain anything else than peers and doesn't have any blank lines.

## Test your setup

To test your setup, you can run the script "amounts.sh", here what it will look like if you have sent 1000 units to your first test address:

```
$ ./amounts.sh 
Requesting amounts on peer me.jytou.fr:9003...
1 Gtest1FwUBXCrUzCL3JY8qJaE1mwcS9Jc5hzzD1wb9Ma = 1000.00
2 GtesT4aEKgii4jimQ5LK3NJgs3gHh4qG2Wge3WhYbBYB = 0.00
3 GTeST4FjBCkJB9ZriZib76VENPWEtbdcn2YimzoPfqvb = 0.00
4 Gtest5g21mqsZZoeSciJk5p4BvG2nXPXz7cP2vM5iC4D = 0.00
...

```

Note that the peer to get the amounts will be selected randomly from the list of peers you have pasted in the "peers.txt" file.

You can also test the send.sh script, for instance, to send 20 units from address 1 to address 2, you would type:

```
./send.sh 1 20 2
```

## Feed the windmill

Of course, you could run the whole thing right now. Most of your transactions would be rejected because your addresses do not have any funds except the first one, but eventually the only transactions from the first address would fill up the other addresses.

A more efficient way is to generate transactions only from the first address(es) that have already been seeded to all the others. There is a command for that:

```
./dispatch.sh 1
```

This command will generate transactions only from the first address to the others. If you have fed more than one address, let's say the 3 first addresses, then you would run:

```
./dispatch.sh 3
```

and all transactions would use the first 3 addresses as sources and any other address as destination.

So you will want to create a lot of transactions to feed the windmill. You can just use cron, or otherwise launch it from the command line.

### Cron way

```
crontab -e
```

and add the line:

```
*/5 * * * * /home/myuser/tgen/dispatch.sh 1
```

to run it every 5 minutes.

### Command line way

Just run something like:

```
for i in `seq 20` ; do ./dispatch.sh 1 ; done
```

This will run the dispatch.sh script 20 times.

# Run it normally

Once all your addresses have enough funds (check it with ./amounts.sh), you can just run ./dispatch.sh without any arguments, and the musical chairs will start dancing! You can again modify your crontab to remove the extra "1" argument.

# Adapting it to your needs

Depending on the total funds you have fed the windmill and the number of addresses you are using, you may want to edit the maximum amount that will be sent in a transaction in the dispatch.sh script:

```
maxsum=50
```

Likewise, the number of transactions to generate every time the script is run can be changed, you could use any formula you want (but this should never be 0):

```
maxnb=$(( `date +%H` + 5 ))
```

That's all folks! Have fun!